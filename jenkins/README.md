# Automatic Jenkins EC2 instance creation

This project creates an Amazon EC2 instance and then installs Jenkins and plugins.

To run this project you will need to modify the environment variables based on the environment **dev** or **prod**.
The variables are located in the group_vars directory in the specific files.

To launch;

```
ansible-playbook -i environments/dev create.yml
```

This command will create a Jenkins Instance using the **dev** environment values.