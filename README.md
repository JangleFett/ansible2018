# Including the submodules

The git-jenkins makes use of some Ansible modules.

To include these into your code you need to do either of the following;

## Cloning

If you are cloning for the first time you'll need to do;

```
git clone --recursive <this repo url>
```

## Already have the repo

You'll need to update the repo by using;

```
git submodule update --init --recursive
```
